(in-package #:regular-language-compiler)

(defparameter *transition-table-example-1* (make-hash-table :test #'equalp))
(let ((table *transition-table-example-1*))
  (setf (gethash #(0 #\1) table) 1)
  (setf (gethash #(0 #\0) table) 0)
  (setf (gethash #(1 #\0) table) 0)
  (setf (gethash #(1 #\1) table) 1))

(defparameter *automata-example-1*
  (make-instance 'finite-automata
		 :start-state 0
		 :alphabet (list #\0 #\1)
		 :states (list 0 1)
		 :end-states (list 1)
		 :transition-table *transition-table-example-1*))

(defparameter *transition-table-example-2* (make-hash-table :test #'equalp))
(let ((table *transition-table-example-2*))
  (setf (gethash #(base #\u) table) 'u)
  (setf (gethash #(base #\d) table) 'd)
  (setf (gethash #(u #\u) table) 'uu)
  (setf (gethash #(u #\d) table) 'base)
  (setf (gethash #(d #\u) table) 'base)
  (setf (gethash #(d #\d) table) 'dd)
  (setf (gethash #(uu #\u) table) 'uuu)
  (setf (gethash #(uu #\d) table) 'u)
  (setf (gethash #(uuu #\u) table) 'uuu)
  (setf (gethash #(uuu #\d) table) 'uu)
  (setf (gethash #(dd #\u) table) 'd)
  (setf (gethash #(dd #\d) table) 'ddd)
  (setf (gethash #(ddd #\u) table) 'dd)
  (setf (gethash #(ddd #\d) table) 'ddd))

(defparameter *automata-example-2*
  (make-instance 'finite-automata
		 :start-state 'base
		 :alphabet (list #\u #\d)
		 :states (list 'base 'u 'd 'uu 'dd 'uuu 'ddd)
		 :end-states (list 'base)
		 :transition-table *transition-table-example-2*))


(defparameter *inputs-example-1*
  (flet ((random-input ()
	   (aref #(#\u #\d) (random 2))))
    (coerce (loop for i below 1000
	       collect (random-input))
	    'string)))

(let ((scanner (eval (compile-automata *automata-example-2*))))
  (benchmark:with-timing (100000)
    (with-input-from-string (input *inputs-example-1*)
      (funcall scanner input))))

(benchmark:with-timing (100000)
  (with-input-from-string (input *inputs-example-1*)
    (scan *automata-example-2* input)))

(defparameter *transition-table-suffix-example-1* (make-hash-table :test #'equalp))
(let ((table *transition-table-suffix-example-1*))
  (setf (gethash #(a #\1) table) 'b)
  (setf (gethash #(b #\0) table) 'a)
  (setf (gethash #(a #\0) table) 'a)
  (setf (gethash #(b #\1) table) 'b))

(defparameter *suffix-example-1*
  (make-instance 'finite-automata
		 :start-state 'a
		 :alphabet (list #\0 #\1)
		 :states (list 'a 'b)
		 :end-states (list 'b)
		 :transition-table *transition-table-suffix-example-1*))

(with-input-from-string (input "001")
  (scan *suffix-example-1* input))

(with-input-from-string (input "0010")
  (scan *suffix-example-1* input))

(with-input-from-string (input "1")
  (scan *suffix-example-1* input))

(with-input-from-string (input "")
  (scan *suffix-example-1* input))

(defparameter *transition-table-prefix-example-1* (make-hash-table :test #'equalp))
(let ((table *transition-table-prefix-example-1*))
  (setf (gethash #(a #\0) table) 'b)
  (setf (gethash #(a #\1) table) 'c)
  (setf (gethash #(b #\0) table) 'b)
  (setf (gethash #(b #\1) table) 'b)
  (setf (gethash #(c #\0) table) 'c)
  (setf (gethash #(c #\1) table) 'c))

(defparameter *prefix-example-1*
  (make-instance 'finite-automata
		 :start-state 'a
		 :alphabet (list #\0 #\1)
		 :states (list 'a 'b 'c)
		 :end-states (list 'c)
		 :transition-table *transition-table-prefix-example-1*))

(with-input-from-string (input "001")
  (scan *prefix-example-1* input))

(with-input-from-string (input "0010")
  (scan *prefix-example-1* input))

(with-input-from-string (input "1")
  (scan *prefix-example-1* input))

(with-input-from-string (input "")
  (scan *prefix-example-1* input))

(with-input-from-string (input "100")
  (scan *prefix-example-1* input))

(defparameter *prefix-and-suffix-example*
  (and-table *suffix-example-1* *prefix-example-1*))

(with-input-from-string (input "")
  (scan *prefix-and-suffix-example* input))

(with-input-from-string (input "0")
  (scan *prefix-and-suffix-example* input))

(with-input-from-string (input "1")
  (scan *prefix-and-suffix-example* input))

(with-input-from-string (input "01")
  (scan *prefix-and-suffix-example* input))

(with-input-from-string (input "10")
  (scan *prefix-and-suffix-example* input))

(with-input-from-string (input "101")
  (scan *prefix-and-suffix-example* input))


;;;; regular-language-compiler.lisp

(in-package #:regular-language-compiler)

(defclass finite-automata ()
  ((start-state :initarg :start-state :reader start-state)
   (alphabet :initarg :alphabet :reader alphabet)
   (states :initarg :states :reader states)
   (end-states :initarg :end-states :reader end-states)
   (transition-table :initarg :transition-table :reader transition-table)))

(defmethod initialize-instance :after ((automata finite-automata) &key)
  (with-slots (start-state end-states states) automata
    (assert (member start-state states :test #'equalp))
    (loop for end-state in end-states
       do (assert (member end-state states :test #'equalp)))))

(defgeneric scan (automata stream))

(defmethod scan ((automata finite-automata) stream)
  (with-slots (start-state transition-table end-states) automata
    (let ((current-state start-state))
      (loop while (peek-char nil stream nil)
	 do (let ((current-char (read-char stream)))
	      (multiple-value-bind (next-state state-found) (gethash (vector current-state current-char) transition-table)
		(assert state-found)
		(setf current-state next-state))))
      (if (member current-state end-states :test #'equalp)
	  (values t current-state)
	  (values nil current-state)))))

(defvar *zero-length-string* (gensym))

(defclass nondeterministic-finite-automata ()
  ((start-state :initarg :start-state :reader start-state)
   (alphabet :initarg :alphabet :reader alphabet)
   (states :initarg :states :reader states)
   (end-states :initarg :end-states :reader end-states)
   (transition-table :initarg :transition-table :reader transition-table)))

(defmethod scan ((automata nondeterministic-finite-automata) stream)
  (with-slots (start-state transition-table end-states) automata
    (let ((current-state-set (list start-state)))
      (loop while (peek-char nil stream nil)
	 do (let ((current-char (read-char stream)))
	      (setf current-state (loop for state in current-state-set
				     append (loop for new-state in (gethash (vector current-state current-char)
									    transition-table)
					       append (gethash (vector current-state *zero-length-string*)
								transition-table))))
	      (push (gethash (vector current-state
	      

(defun cartesian-product (first-sequence second-sequence)
  (loop for first-item in first-sequence
     append (loop for second-item in second-sequence
	       collect (vector first-item second-item))))

(defun transition-table-product (first-table second-table)
  (let ((product (make-hash-table :test #'equalp)))
    (loop for first-key being the hash-keys in first-table using (hash-value first-value)
       do (loop for second-key being the hash-keys in second-table using (hash-value second-value)
	     do (setf (gethash (vector first-key second-key) product) (vector first-value second-value))))
    product))

(defmethod and-table ((first-automata finite-automata) (second-automata finite-automata))
  (assert (equalp (alphabet first-automata) (alphabet second-automata)))
  (make-instance 'finite-automata
		 :start-state (vector (start-state first-automata)
				      (start-state second-automata))
		 :alphabet (alphabet first-automata)
		 :states (cartesian-product
			  (states first-automata)
			  (states second-automata))
		 :end-states (cartesian-product
			      (end-states first-automata)
			      (end-states second-automata))
		 :transition-table (let ((table (make-hash-table :test #'equalp)))
				     (loop for input in (alphabet first-automata)
					do (loop for first-state in (states first-automata)
					      do (loop for second-state in (states second-automata)
						    do (setf (gethash (vector (vector first-state second-state) input) table)
							     (vector (gethash (vector first-state input)
									      (transition-table first-automata))
								     (gethash (vector second-state input)
									      (transition-table second-automata)))))))
				     table)))

(defmethod or-table ((first-automata finite-automata) (second-automata finite-automata))
  (assert (equalp (alphabet first-automata) (alphabet second-automata)))
  (make-instance 'finite-automata
		 :start-state (vector (start-state first-automata)
				      (start-state second-automata))
		 :alphabet (alphabet first-automata)
		 :states (cartesian-product
			  (states first-automata)
			  (states second-automata))
		 :end-states (union (cartesian-product (end-states first-automata)
						       (states second-automata))
				    (cartesian-product (states first-automata)
						       (end-states second-automata)))
		 :transition-table (transition-table-product
				    (transition-table first-automata)
				    (transition-table second-automata))))



(with-input-from-string (input "udud")
  (symbol-macrolet ((value (read-char input nil 'eof-value)))
    (block escape
      (tagbody
       base (ecase value
	      (#\u (go u))
	      (#\d (go d))
	      (eof-value (return-from escape 'base)))
       u (ecase value
	   (#\u (go uu))
	   (#\d (go base))
	   (eof-value (return-from escape 'u)))
       d (ecase value
	   (#\u (go base))
	   (#\d (go dd))
	   (eof-value (return-from escape 'd)))
       uu (ecase value
	    (#\u (go uuu))
	    (#\d (go d))
	    (eof-value (return-from escape 'uu)))
       dd (ecase value
	    (#\u (go d))
	    (#\d (go ddd))
	    (eof-value (return-from escape 'dd)))
       uuu (ecase value
	     (#\u (go uuu))
	     (#\d (go uu))
	     (eof-value (return-from escape 'uuu)))
       ddd (ecase value
	     (#\u (go dd))
	     (#\d (go ddd))
	     (eof-value (return-from escape 'ddd)))))))

(defun compile-automata (automata)
  (let ((escape (gensym "ESCAPE-"))
	(eof-value (gensym "EOF-VALUE-")))
    `(lambda (input)
       (declare (optimize (safety 0) (speed 3)))
       (block ,escape
	 (tagbody
	    ,@(loop for state in (states automata)
		 append (list state
			      `(ecase (read-char input nil ',eof-value)
				 ,@(loop for input in (alphabet automata)
				      collect `(,input (go ,(gethash (vector state input) (transition-table automata)))))
				 (',eof-value (return-from ,escape ',state))))))))))


